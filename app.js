// Global elements taken from the html
const balanceElement = document.getElementById("balance");
const loanElement = document.getElementById("loan");
const payElement = document.getElementById("pay");
const costElement = document.getElementById("cost");
const laptopsElement = document.getElementById("laptops");
const specsLaptopElement = document.getElementById("laptop-specs");
const imageElement = document.getElementById("laptop-image");
const descriptionLaptopElement = document.getElementById("laptop-description");
const getLoanElement = document.getElementById("get-loan-button");
const bankPayElement = document.getElementById("bank-button");
const workPayElement = document.getElementById("work-button");
const repayLoanElement = document.getElementById("repay-button");
const buyElement = document.getElementById("buy-button");

// Global variables
let currentBalance = 0;
let currentPay = 0;
let currentLoan = 0;
let laptops = [];

// Fetching of the data from the API
fetch(`https://noroff-komputer-store-api.herokuapp.com/computers`)
  .then((response) => response.json())
  .then((data) => (laptops = data))
  .then((laptops) => addLaptopsToList(laptops));

function addLaptopsToList(laptops) {
  // Sets up the first item displayed on the page
  setUpData();

  // Populates the list of items available
  laptops.forEach((x) => addLaptopToList(x));
}

// Adds each list item
function addLaptopToList(laptop) {
  const laptopElement = document.createElement("option");
  laptopElement.value = laptop.id;
  laptopElement.appendChild(document.createTextNode(laptop.title));
  laptopsElement.appendChild(laptopElement);
}

function setUpData() {
  // Sets up the description, price, image and the
  // features for the first item to display

  balanceElement.innerText = `Balance : ${currentBalance} Kr`;
  payElement.innerText = `Pay : ${currentPay}`;
  loanElement.innerText = `Loan : ${currentLoan} Kr`;

  descriptionLaptopElement.innerText = laptops[0].description;
  costElement.innerText = `${laptops[0].price} KR`;
  imageElement.src =
    "https://noroff-komputer-store-api.herokuapp.com/" + laptops[0].image;
  laptops[0].specs.forEach((x) => listFeatures(x));

  // Hides the loan text and repay loan button at the start
  repayLoanElement.style.visibility = `hidden`;
  loanElement.style.visibility = `hidden`;
}

// Populates the features list for the current laptop
function listFeatures(laptopSpecs) {
  const specs = document.createElement("li");
  specs.innerText = laptopSpecs;
  specsLaptopElement.appendChild(specs);
}

// Responsible for updating the text on the html side
// Hides elements if needed
function updateInnerText() {
  balanceElement.innerText = `Balance : ${currentBalance} Kr`;
  loanElement.innerText = `Loan : ${currentLoan} Kr`;
  payElement.innerText = `Pay : ${currentPay}`;
  // Hides the loan and repay loan element if not loan exist
  if (currentLoan == 0) {
    repayLoanElement.style.visibility = `hidden`;
    loanElement.style.visibility = `hidden`;
  } else {
    repayLoanElement.style.visibility = `visible`;
    loanElement.style.visibility = `visible`;
  }
}

// Block of code related to the "Joe Banker"

// Handles the loan of the bank
function handleLoan() {
  // Checks if there is a loan currently in place, if so, prevents loaning more money
  if (currentLoan !== 0) {
    alert(`Repay your loan first!`);
  } else {
    const loanAmount = prompt("Please enter the amount you want to loan");
    const amount = parseInt(loanAmount);
    // Makes sure that the amount entered is an actual number
    if (!isNaN(amount)) {
      if (amount > 0) {
        // Checks if the amount being withdrawn is double the current balance
        if (amount > currentBalance * 2) {
          alert(
            `You are not allowed to loan anything above double the amount of balance you have`
          );
        } else {
          currentBalance += amount;
          currentLoan = amount;

          updateInnerText();
        }
      } else {
        alert(`You can not borrow nothing/negative money`);
      }
    } else {
      alert(`Input an actual number!`);
    }
  }
}

// Block of code related to "Work"

// Handle banking money made from working with a 10% "tax"
// if a loan has been taken
function handleBankPay() {
  // Checks if there is money to be banked
  if (currentPay > 0) {
    // checks if there is a loan taken and if so, take 10% from the
    // pay and use it to pay of the loan
    if (currentLoan > 0) {
      let tax = currentPay * 0.1;
      if (currentLoan - tax < 0) {
        currentBalance += currentPay - currentLoan;
        currentLoan = 0;
        currentPay = 0;
      } else {
        currentLoan -= tax;
        currentBalance += currentPay - tax;
        currentPay = 0;
      }
    } else {
      currentBalance += currentPay;
      currentPay = 0;
    }

    updateInnerText();
  } else {
    alert(`No money to deposit, work more`);
  }
}

// Adds money to pay each time the button is pressed
function handleWorkPay() {
  currentPay += 100;
  updateInnerText();
}

// Handles if a loan has been taken and
// User presses the repay loan button
function handleRepayLoan() {
  // Checks if the current pay is smaller then the current loan
  // if so, the use all the funds to pay the loan
  if (currentPay <= currentLoan && currentPay !== 0) {
    currentLoan -= currentPay;
    currentPay = 0;
  }
  // If there are no money in the pay,
  // alerts that no money can be used
  else if (currentPay === 0) {
    alert(`No money to use`);
  }
  // Uses the available funds to pay the loan 
  // and leaves the remaining amount in the pay 
  else {
    currentPay -= currentLoan;
    currentLoan = 0;
  }
  updateInnerText();
}

// Code related to laptops

// Handles the change of selection of laptops in the list 
function handleLaptopChange(e) {
  const selectedLaptop = laptops[e.target.selectedIndex];
  descriptionLaptopElement.innerText = selectedLaptop.description;
  costElement.innerText = `${selectedLaptop.price} KR`;

  // As the API contains a curveball with the image, the laptop
  // with id == 5 is a different format, therefore a specific solution
  // has been put in place, 
  // If laptop id 5 is loaded, load the specific url
  // otherwise load the selected laptop + the image api in the the img-element
  if (selectedLaptop.id == 5) {
    imageElement.src =
      "https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png";
  } else {
    imageElement.src =
      "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image;
  }

  // Clears the list of features from the li tag before adding new text
  clearList();
  selectedLaptop.specs.forEach((x) => listFeatures(x));
}

// Handles the buying of a laptop
function handleBuy() {
  const selectedLaptop = laptops[laptopsElement.selectedIndex];
  // Checks if necessary funds are available for the selected laptop
  if (currentBalance >= selectedLaptop.price) {
    currentBalance -= selectedLaptop.price;
    alert(`${selectedLaptop.title} Bought`);
  } else {
    alert(`Not enough balance`);
  }

  updateInnerText();
}

// Clear the specs/features to make room
function clearList() {
  while (specsLaptopElement.lastChild) {
    specsLaptopElement.removeChild(specsLaptopElement.lastChild);
  }
}

// Code related to the events on the page
getLoanElement.addEventListener("click", handleLoan);
bankPayElement.addEventListener("click", handleBankPay);
workPayElement.addEventListener("click", handleWorkPay);
repayLoanElement.addEventListener("click", handleRepayLoan);
buyElement.addEventListener("click", handleBuy);
laptopsElement.addEventListener("change", handleLaptopChange);
